# README #

### What is this? ###

This is a splunk app that helps maintain correlation searches in Splunk Enterprise Security.

Check out my site to learn more about it: https://www.gabrielvasseur.com/post/maintaining-your-correlation-searches-with-es-choreographer

### How do I get set up? ###

1. you need to install GV-Utils: https://splunkbase.splunk.com/app/6269
1. Then install SA-ES-Choreographer
1. Navigate to the app and clik on the setup dashboard: click the button and let all the searches run
1. Edit your SplunkEnterpriseSecurity navigation menuand add:  
  
  
```
  <collection label="ES Choreographer">
    <view name="morning_checks_checker"/>
    <view name="correlation_searches_best_practices"/>
    <view name="correlation_searches_best_practices_evolution"/>
    <view name="status_of_correlation_searches"/>
    <divider/>
    <view name="incident_review_fields"/>
    <view name="workflow_actions"/>
  </collection>
```

### So where is it? ###

This app is now on splunkbase: [ES Choreographer](https://splunkbase.splunk.com/app/6309)